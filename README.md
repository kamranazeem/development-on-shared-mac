# Using a shared mac for development

MAC computer: 192.168.0.54

## Local setup - Enable remote access, remote login:
Login on your mac directly on it's console as admin user `kamran`. Then, go to mac -> System Preferences -> Sharing, and then enable the following two:

* Screen Sharing ( Allow access for **All users**) (Also select: "Anyone may request permission to control screen")
* Remote Login ( Allow access for **All users** )


Also, prevent this computer from sleeping. Go to  mac -> System Preferences -> Battery -> Power Adapter;  and select "Prevent computer from sleeping automatically when the display is off". There is also a command line alternative for this operation.


## SSH settings:

In the beginning you would need to login over SSH as an admin user `kamran` without using keys. This is the default SSHD behavior on mac.


Copy your SSH ID files to the main admin user `kamran` from your local linux (or windows) computer, using the command below:

```
ssh-copy-id kamran@192.168.0.54
```

Once your ssh ID files are copied over to mac, verify that you can access user `kamran`on mac, using key based authentication. For that, first disable password based login on mac:


```
vi /private/etc/ssh/sshd_config

# To enable only key-based access:
PasswordAuthentication no
ChallengeResponseAuthentication no
```

Restart SSHD on mac:

```
sudo launchctl stop com.openssh.sshd

sudo launchctl start com.openssh.sshd
```


Verify that you can login with ssh keys instead of password. 
```
[kamran@kworkhorse keys]$ ssh kamran@192.168.0.54
Last login: Tue Dec 29 00:48:20 2020 from 192.168.0.52
kamran@Kamran-MacBook-Pro ~ % sudo -i
Password:
Kamran-MacBook-Pro:~ root#
```
**Note:** My ssh key is passphrase protected, but the above command did not ask for the passphrase! This is because I already accessed some servers using my ssh key, and GNOME keyring manager has kept it the passphrase in memory after asking it once.


Alright, so we are in mac using SSH.

Disable some "Dock" effects to improve performance of the display over VNC.

(Screenshot here)


## Login to mac over VNC:

From you Linux or windows computer, login to mac computer, using a VNC client. On Linux I find Remmina to be the best VNC, and RDP client. On Windows, I could only get RealVNC to work with mac os. You are free to use any other software you want. RealVNC had poor performance though.

(Remmina settings screenshots)


Once connected over VNC, you should see you MAC screen. You may be asked once more to login on the mac VNC screen, which is OK.

After logging in, you may see that the VNC display is not fitting on your local computer screen. On left side of Remmina windows, there is a button named **"Toggle scaled mode"**. Press this, and mac's VNC display will scale to your local computer's screen.

Then, press the "Toggle fullscreen mode" to make it full screen, which should be easier (and faster) to use.

(screenshot - unscaled)

(screenshot - scaled)
 

## Install additional software:

### Install Homebrew as user `kamran` - without sudo:
```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```


Then, install other software.

### Install git:

```
brew install git
```

```
kamran@Kamran-MacBook-Pro ~ % git version
git version 2.24.3 (Apple Git-128)
kamran@Kamran-MacBook-Pro ~ % 
```

### Install Xcode:

Ref: [https://www.freecodecamp.org/news/how-to-download-and-install-xcode/](https://www.freecodecamp.org/news/how-to-download-and-install-xcode/)

* You'll need a good, stable internet connection. 
* The latest version is around 8 gigabytes in size.
* Be sure to have at least 30 gigabytes of free space on your computer. The latest .xip file (v11.4.1 at the time of writing) is ~8 gigabytes zipped. When you unzip it, that's another 17 gigabytes. Then you'll need the command line tool, which is yet another 1.5 gigabytes.
* If you have multiple users on your computer, you will need to update the CLT for each user.

(so silly. Why does it need so much of space?)








